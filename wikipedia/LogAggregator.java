package aggregator;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Vector;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;

/**
 * @author Virginie Galtier
 *
 *         Reads the Kafka topic that publishes the locale of Wikipedia changes
 *         as strings, identifies and write to a file the 5-top active regions on tumbling
 *         windows.
 */
public class LogAggregator {

	/**
	 * size of the leader board
	 */
	private int NB_REGIONS_TO_LOG = 5;

	/**
	 * Creates the aggregator (provoking infinite execution).
	 * 
	 * @param args first argument is a list of Kafka bootstrap servers, 
	 *             second argument is the name of the source Kafka topic,
	 *             third argument is the size of the leader board
	 *             fourth argument is the file name
	 */
	public static void main(String[] args) {
		new LogAggregator(args[0], args[1], Integer.parseInt(args[2]), args[3]);
	}

	/**
	 * 
	 * @param bootstrapServers List of Kafka bootstrap servers. Example:
	 *                         localhost:9092,another.host:9092
	 * @param regionTopicName  Name of the source Kafka topic
	 * @param windowsSec       Width of the tumbling window, in seconds
	 * @param logFileName      Name of the output log file
	 */
	public LogAggregator(String bootstrapServers, String regionTopicName, int windowsSec, String logFileName) {

		KafkaConsumer<Void, String> consumer = new KafkaConsumer<Void, String>(
				configureKafkaConsumer(bootstrapServers));
		// version 1
		// read from any partition of the topic
		// (and hopefully all of them! should be alone in its group!!!)
		// consumer.subscribe(Collections.singletonList(localeTopicName));

		// version 2
		// comment out the following line in the configureKafkaConsumer method:
		// consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_aggregator");
		// Get metadata about partitions for all topics that the user is authorized to
		// view.
		Map<String, List<PartitionInfo>> topicsMap = consumer.listTopics();
		// Focus on the locale topic
		List<PartitionInfo> regionPartitions = topicsMap.get(regionTopicName);
		// add each partition to the list of partitions to assign
		Collection<TopicPartition> partitions = new Vector<TopicPartition>();
		for (int i = 0; i < regionPartitions.size(); i++) {
			partitions.add(new TopicPartition(regionTopicName, regionPartitions.get(i).partition()));
		}
		consumer.assign(partitions);

		/*
		 * list of the regions subject to changes along with the number of changes 
		 * key: region, value: number of times this locale came up in the topic
		 */
		Map<String, Integer> regions = new HashMap<String, Integer>();

		long timeStone = System.currentTimeMillis();

		try {
			Duration timeout = Duration.ofMillis(1000);
			while (true) {
				// reads from topic
				ConsumerRecords<Void, String> records = consumer.poll(timeout);
				for (ConsumerRecord<Void, String> record : records) {
					String region = record.value();
					if (regions.containsKey(region)) {
						regions.put(region, 1 + regions.get(region));
					} else {
						regions.put(region, 1);
					}

					// if time has come, displays the statistics and starts afresh
					if (System.currentTimeMillis() - timeStone > windowsSec * 1000) {
						timeStone = System.currentTimeMillis();
						displayAndLogTop(regions, logFileName);
						regions = new HashMap<String, Integer>();
					}
				}
			}
		} catch (Exception e) {
			System.out.println("something went wrong... " + e.getMessage());
		} finally {
			consumer.close();
		}
	}

	private void displayAndLogTop(Map<String, Integer> regions, String logFileName) {
		// sorts the region list in reverseOrder (locale key with the highest value
		// first)
		Iterator<Entry<String, Integer>> regionIterator = regions.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).iterator();

		int j = 0;
		Entry<String, Integer> entry = null;

		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(logFileName, true); // append = true
			while ((regionIterator.hasNext()) && (j < NB_REGIONS_TO_LOG)) {
				entry = regionIterator.next();
				System.out.println(entry.getKey() + "\t\t" + entry.getValue());
				fileWriter.write(entry.getKey() + "\t\t" + entry.getValue() +"\n");
				j++;
			}
			System.out.println("=================================");
			fileWriter.write("================================="+"\n");
			fileWriter.close();
		} catch (IOException e) {
			System.err.println("problem to write results to file: " + e.getMessage());
		}
	}

	/**
	 * Prepares configuration for the Kafka consumer.
	 * 
	 * @param bootstrapServers the list of Kafka bootstrapping servers
	 * @return configuration properties for the Kafka consumer
	 */
	private Properties configureKafkaConsumer(String bootstrapServers) {
		Properties consumerProperties = new Properties();

		consumerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
		consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.VoidDeserializer.class.getName());
		consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
				org.apache.kafka.common.serialization.StringDeserializer.class.getName());
		// consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, "the_aggregator");
		consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest"); // from beginning

		return consumerProperties;
	}
}
